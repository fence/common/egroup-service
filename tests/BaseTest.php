<?php

declare(strict_types=1);

namespace Glance\EgroupService\Tests;

use PHPUnit\Framework\TestCase;
use Glance\EgroupService\Egroup\Infrastructure\EgroupProvider;

abstract class BaseTest extends TestCase
{
    public static $groupProvider;

    public static function setUpBeforeClass(): void
    {
        self::$groupProvider = EgroupProvider::createWithUserCredentials(
            getenv("USER_LOGIN"),
            getenv("USER_PASSWORD"),
        );
    }

    public static function getRandomString(
        int $length = 64,
        string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
}
