<?php

declare(strict_types=1);

namespace Glance\EgroupService\Tests\Integration;

use Glance\EgroupService\Egroup\Domain\Account;
use Glance\EgroupService\Egroup\Domain\ArchiveProperties;
use Glance\EgroupService\Egroup\Domain\Egroup;
use Glance\EgroupService\Egroup\Domain\EgroupWithPrivileges;
use Glance\EgroupService\Egroup\Domain\EmailProperties;
use Glance\EgroupService\Egroup\Domain\MailPostingRestrictions;
use Glance\EgroupService\Egroup\Domain\MaxMailSize;
use Glance\EgroupService\Egroup\Domain\Owner;
use Glance\EgroupService\Egroup\Domain\Person;
use Glance\EgroupService\Egroup\Domain\PostingRestrictions;
use Glance\EgroupService\Egroup\Domain\Privacy;
use Glance\EgroupService\Egroup\Domain\SelfSubscription;
use Glance\EgroupService\Egroup\Domain\SelfSubscriptionEgroup;
use Glance\EgroupService\Egroup\Domain\StaticEgroup;
use Glance\EgroupService\Egroup\Domain\Type;
use Glance\EgroupService\Egroup\Domain\Usage;
use Glance\EgroupService\Egroup\Domain\WhoReceivesDeliveryErrors;
use Glance\EgroupService\Tests\BaseTest;

final class EgroupProviderTest extends BaseTest
{
    public function testSynchronizeNewGroup(): string
    {
        $personIdsData = [382894, 840720];
        $egroupMembersData = [getenv("TEST_STATIC_EGROUP_NAME")];
        $accountMembersData = [getenv("TEST_ACCOUNT_NAME")];
        $ownerPersonId = 851203;
        $allowedToPostPersonId = 848592;

        $members = array_merge(
            array_map(function (int $personId) {
                return Person::create($personId);
            }, $personIdsData),
            array_map(function (string $egroupMemberName) {
                return StaticEgroup::create($egroupMemberName);
            }, $egroupMembersData),
            array_map(function (string $accountMemberName) {
                return Account::create($accountMemberName);
            }, $accountMembersData),
        );

        $emailProperties = EmailProperties::create(
            MailPostingRestrictions::create(
                $postingRestrictions = PostingRestrictions::ownerAdminsAndOthers(),
                $otherRecipientsAllowedToPost = [
                    Person::create($allowedToPostPersonId)
                ],
            ),
            $senderAuthenticationEnabled = false,
            $whoReceivesDeliveryErrors = WhoReceivesDeliveryErrors::sender(),
            $maxMailSize = MaxMailSize::tenMb(),
            $archiveProperties = ArchiveProperties::active()
        );

        $egroup = Egroup::create(
            $name = getenv("TEST_EXPERIMENT_PREFIX") . "-egroup-service-test-" . self::getRandomString(5),
            $topic = "LHCb",
            $owner = Owner::create($ownerPersonId),
            $members,
            $adminEgroup = null,
            $description = "This was created as a test",
            $type = Type::staticEgroup(),
            $usage = Usage::securityMailing(),
            $privacy = Privacy::members(),
            $selfSubscription = SelfSubscription::members(),
            $emailProperties,
            $egroupsWithPrivileges = [
                EgroupWithPrivileges::createWithAdminPrivilegeFromString(getenv("TEST_STATIC_EGROUP_NAME")),
            ],
            $selfSubscriptionEgroups = [
                SelfSubscriptionEgroup::createWithApprovalNotNeededFromId((int) getenv("TEST_STATIC_EGROUP_ID")),
            ]
        );

        $syncEgroupResponse = self::$groupProvider->synchronizeEgroup($egroup);
        $this->assertEmpty($syncEgroupResponse->warnings());

        $findEgroupResponse = self::$groupProvider->findEgroupByName($name);

        $this->assertEmpty($findEgroupResponse->warnings());
        $this->assertNotNull($findEgroupResponse->egroup());

        /** @var Egroup */
        $egroup = $findEgroupResponse->egroup();

        $this->assertEquals($name, $egroup->name());
        $this->assertEquals($ownerPersonId, $egroup->owner()->toArray()["PersonId"]);
        $this->assertEquals($description, $egroup->description());
        $this->assertEquals($type->toString(), $egroup->type()->toString());
        $this->assertEquals($usage->toString(), $egroup->usage()->toString());
        $this->assertEquals($privacy->toString(), $egroup->privacy()->toString());
        $this->assertEqualsCanonicalizing(
            $personIdsData,
            array_values(array_filter(array_map(function (object $member) {
                if ($member->type() === Person::TYPE_NAME) {
                    return $member->personId();
                }
                return null;
            }, $egroup->members()))),
        );
        $this->assertEqualsCanonicalizing(
            $egroupMembersData,
            array_values(array_filter(array_map(function (object $member) {
                if ($member->type() === StaticEgroup::TYPE_NAME) {
                    return $member->name();
                }
                return null;
            }, $egroup->members()))),
        );
        $this->assertEqualsCanonicalizing(
            $accountMembersData,
            array_values(array_filter(array_map(function (object $member) {
                if ($member->type() === Account::TYPE_NAME) {
                    return $member->name();
                }
                return null;
            }, $egroup->members()))),
        );
        $this->assertEquals(
            $selfSubscription->toString(),
            $egroup->selfSubscription()->toString()
        );
        $this->assertEqualsCanonicalizing(
            [getenv("TEST_STATIC_EGROUP_ID")],
            array_values(array_filter(array_map(function (object $selfSubscriptionEgroup) {
                return $selfSubscriptionEgroup->id();
            }, $egroup->selfSubscriptionEgroups()))),
        );
        $this->assertEqualsCanonicalizing(
            [getenv("TEST_STATIC_EGROUP_NAME")],
            array_values(array_filter(array_map(function (object $egroupsWithPrivilege) {
                return $egroupsWithPrivilege->name();
            }, $egroup->egroupsWithPrivileges()))),
        );
        $this->assertEquals(
            $postingRestrictions->toString(),
            $egroup->emailProperties()->mailPostingRestrictions()->postingRestrictions()->toString()
        );
        $this->assertEqualsCanonicalizing(
            [$allowedToPostPersonId],
            array_values(array_filter(array_map(function (object $member) {
                if ($member->type() === Person::TYPE_NAME) {
                    return $member->personId();
                }
                return null;
            }, $egroup->emailProperties()->mailPostingRestrictions()->otherRecipientsAllowedToPost()))),
        );
        $this->assertEquals(
            $senderAuthenticationEnabled,
            $egroup->emailProperties()->senderAuthenticationEnabled()
        );
        $this->assertEquals(
            $whoReceivesDeliveryErrors->toString(),
            $egroup->emailProperties()->whoReceivesDeliveryErrors()->toString()
        );
        $this->assertEquals(
            $maxMailSize->toString(),
            $egroup->emailProperties()->maxMailSize()->toString()
        );
        $this->assertEquals(
            $archiveProperties->toString(),
            $egroup->emailProperties()->archiveProperties()->toString()
        );

        return $egroup->name();
    }

    /**
     * @depends testSynchronizeNewGroup
     */
    public function testSynchronizeExistingGroup(string $egroupName): string
    {
        $personId = 382894;
        $egroupMemberName = getenv("TEST_STATIC_EGROUP_NAME");

        $membersData = [
            Person::create($personId),
            StaticEgroup::create($egroupMemberName)
        ];

        $findEgroupResponse = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($findEgroupResponse->warnings());
        $this->assertNotNull($findEgroupResponse->egroup());

        $egroup = $findEgroupResponse->egroup();

        $egroup->updateMembers($membersData);

        $syncEgroupResponse = self::$groupProvider->synchronizeEgroup($egroup);

        $this->assertEmpty($syncEgroupResponse->warnings());

        $findEgroupResponse = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($findEgroupResponse->warnings());
        $this->assertNotNull($findEgroupResponse->egroup());

        $egroup = $findEgroupResponse->egroup();

        $this->assertContains(
            $personId,
            array_filter(array_map(function (object $member) {
                if ($member->type() === Person::TYPE_NAME) {
                    return $member->personId();
                }
                return null;
            }, $egroup->members()))
        );
        $this->assertContains(
            $egroupMemberName,
            array_filter(array_map(function (object $member) {
                if ($member->type() === StaticEgroup::TYPE_NAME) {
                    return $member->name();
                }
                return null;
            }, $egroup->members())),
        );
        return $egroup->name();
    }

    /**
     * @depends testSynchronizeExistingGroup
     */
    public function testAddMembers(string $egroupName): string
    {
        $personId = 851203;

        $members = [
            Person::create($personId)
        ];

        $response = self::$groupProvider->addEgroupMembers(
            $egroupName,
            $members
        );

        $this->assertEmpty($response->warnings());

        $response = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($response->warnings());
        $this->assertNotNull($response->egroup());
        $egroup = $response->egroup();

        $this->assertContains(
            $personId,
            array_filter(array_map(function (object $member) {
                if ($member->type() === Person::TYPE_NAME) {
                    return $member->personId();
                }
                return null;
            }, $egroup->members())),
        );

        return $egroup->name();
    }

    /**
     * @depends testAddMembers
     */
    public function testRemoveMembers(string $egroupName): string
    {
        $personId = 851203;

        $members = [
            Person::create($personId)
        ];

        $response = self::$groupProvider->removeEgroupMembers(
            $egroupName,
            $members
        );

        $this->assertEmpty($response->warnings());

        $response = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($response->warnings());
        $this->assertNotNull($response->egroup());
        $egroup = $response->egroup();

        $this->assertNotContains(
            $personId,
            array_filter(array_map(function (object $member) {
                if ($member->type() === Person::TYPE_NAME) {
                    return $member->personId();
                }
                return null;
            }, $egroup->members())),
        );

        return $egroup->name();
    }

    /**
     * @depends testRemoveMembers
     */
    public function testUpdateEmailProperties(string $egroupName): string
    {
        $findEgroupResponse = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($findEgroupResponse->warnings());
        $this->assertNotNull($findEgroupResponse->egroup());

        $egroup = $findEgroupResponse->egroup();
        $egroup->emailProperties()->updateMaxMailSize(MaxMailSize::fiveMb());

        $response = self::$groupProvider->updateEmailProperties(
            $egroup
        );

        $this->assertEmpty($response->warnings());

        $findEgroupResponse = self::$groupProvider->findEgroupByName($egroupName);

        $this->assertEmpty($findEgroupResponse->warnings());
        $this->assertNotNull($findEgroupResponse->egroup());

        $egroup = $findEgroupResponse->egroup();

        $this->assertEquals(
            MaxMailSize::fiveMb()->toString(),
            $egroup->emailProperties()->maxMailSize()->toString()
        );
        return $egroup->name();
    }

    /**
     * @depends testSynchronizeNewGroup
     */
    public function testDeleteEgroup(string $egroupName): void
    {
        $response = self::$groupProvider->delete($egroupName);
        $this->assertEmpty($response->warnings());
    }
}
