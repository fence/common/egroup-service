# E-groups Service

This is an abstraction on top of the CERN e-groups web service.

## Getting started

The official documentation can be found on the following URLs:

- Documentation: https://aisdocs.web.cern.ch/display/ED/6.+Web+Service
- WSDL: https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl
- Schema: https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsServicesSchema.xsd

## Usage

The e-group provider is created with the user credentials.

```php
$egroupProvider = EgroupProvider::createWithUserCredentials(
    $user         = getenv("USER"),
    $password     = getenv("PASSWORD"),
);
```

Every method returns a `Response` object. This object has the following methods:

- `transactionId()` - Returns the id of the transaction.
- `warnings()` - Returns an array of warnings. Empty by default.
- `egroup()` - Returns an `Egroup` object (when applicable). Null by default.

Any error thrown will be an instance of `EgroupException`. The error message is the message returned by the web service.

#### `EgroupProvider::findEgroupByName()`

This method accepts a string (the e-group name) as parameter.

```php
$response = $egroupProvider->findEgroupByName("lhcb-glance-admins");

echo($response->egroup()->name());

// Output:
// lhcb-glance-admins
```

#### `EgroupProvider::synchronizeEgroup()`

The `synchronizeEgroup` method can be used to create or update an e-group. The method accepts an `Egroup` object as parameter.'

Creating an e-group:
```php
$egroup = Egroup::create(
    $name = "foo-bar",
    $topic = "LHCb",
    $owner = Owner::create(851203),
    $members = [
        Person::create(382894),
        StaticEgroup::create("lhcb-static-glance-admins")
        DynamicEgroup::create("lhcb-dynamic-glance-admins")
    ],
    $adminEgroup = "lhcb-glance-admins",
    $description = "This was created as a test",
    $type = Type::staticEgroup(),
    $usage = Usage::securityMailing(),
    $privacy = Privacy::members(),
    $selfSubscription = SelfSubscription::members(),
    $emailProperties = EmailProperties::create(
        MailPostingRestrictions::create(
            $postingRestrictions = PostingRestrictions::ownerAdminsAndOthers(),
            $otherRecipientsAllowedToPost = [
                Person::create(840720)
            ],
        ),
        $senderAuthenticationEnabled = false,
        $whoReceivesDeliveryErrors = WhoReceivesDeliveryErrors::sender(),
        $maxMailSize = MaxMailSize::tenMb(),
        $archiveProperties = ArchiveProperties::active()
    );
    $egroupsWithPrivileges = [
        EgroupWithPrivileges::createWithAdminPrivilegeFromString("lhcb-fence-support"),
    ],
    $selfSubscriptionEgroups = [
        SelfSubscriptionEgroup::createWithApprovalNotNeededFromId(10215717),
    ]
);

$response = $egroupProvider->synchronizeEgroup($egroup);

echo($response->egroup()->name());

// Output:
// foo-bar
```

Updating an e-group:
```php
$response = $egroupProvider->findEgroupByName("foo-bar");

$egroup = $response->egroup();

$egroup->updateMembers([
    Person::create(851203)
]);

$response = $egroupProvider->synchronizeEgroup($egroup);

echo(count($response->egroup()->members()));
echo($response->egroup()->members()[0]->personId());

// Output:
// 1
// 851203
```

#### `EgroupProvider::updateEmailProperties()`

The method accepts an `Egroup` object as parameter.'

```php
$response = $egroupProvider->findEgroupByName("foo-bar");

$egroup = $response->egroup();

$egroup->emailProperties()->updateMaxMailSize(MaxMailSize::fiveMb());

$response = $egroupProvider->updateEmailProperties($egroup);

echo($response->egroup()->emailProperties()->maxMailSize()->toString());

// Output:
// 5
```

#### `EgroupProvider::addEgroupMembers()`

The method accepts a string (the e-group name), an array (this array can contain `Person`, `StaticEgroup` and `DynamicEgroup` objects) and the `overwriteMembers` (boolean) as parameters. If `overwriteMembers` is set to `true`, the members of the e-group will be overwritten with the members passed as parameter. If `overwriteMembers` is set to `false`, the members passed as parameter will be added to the e-group. It is `false` by default.

```php
$members = [
    Person::create(382894),
];

$response = $egroupProvider->addEgroupMembers("foo-bar", $members, true);

echo(count($response->egroup()->members()));
echo($response->egroup()->members()[0]->personId());

// Output:
// 1
// 382894
```

#### `EgroupProvider::removeEgroupMembers()`

The method accepts a string (the e-group name), an array as parameters (this array can contain `Person`, `StaticEgroup` and `DynamicEgroup` objects).

```php
$members = [
    Person::create(382894),
];

$response = $egroupProvider->removeEgroupMembers("foo-bar", $members);

echo(count($response->egroup()->members()));

// Output:
// 0
```

#### `EgroupProvider::delete()`

The method accepts a string (the e-group name) as parameter.

```php
$response = $egroupProvider->delete("foo-bar");

echo($response->egroup()));

// Output:
// Null
```
