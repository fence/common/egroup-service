<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class ArchiveProperties
{
    /** @var string */
    private $archiveProperties;

    /** @var string[] */
    public static $allowedPolicies = [
        "DoesNotExist",
        "Active",
        "NotActive"
    ];

    private function __construct(string $archiveProperties)
    {
        if (!in_array($archiveProperties, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Archive properties  should be one of those values: {$allowed}"
            );
        }

        $this->archiveProperties = $archiveProperties;
    }

    public static function doesNotExist(): self
    {
        return new self("DoesNotExist");
    }

    public static function active(): self
    {
        return new self("Active");
    }

    public static function notActive(): self
    {
        return new self("NotActive");
    }

    public static function fromString(string $archiveProperties): self
    {
        return new self($archiveProperties);
    }

    public function toString(): string
    {
        return $this->archiveProperties;
    }
}
