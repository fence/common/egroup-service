<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class WhoReceivesDeliveryErrors
{
    /** @var string */
    private $whoReceivesDeliveryErrors;

    /** @var string[] */
    public static $allowedPolicies = [
        "GroupOwner",
        "Sender",
        "None"
    ];

    private function __construct(string $whoReceivesDeliveryErrors)
    {
        if (!in_array($whoReceivesDeliveryErrors, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Who receives delivery errors should be one of those values: {$allowed}"
            );
        }

        $this->whoReceivesDeliveryErrors = $whoReceivesDeliveryErrors;
    }

    public static function groupOwner(): self
    {
        return new self("GroupOwner");
    }

    public static function sender(): self
    {
        return new self("Sender");
    }

    public static function none(): self
    {
        return new self("None");
    }

    public static function fromString(string $whoReceivesDeliveryErrors): self
    {
        return new self($whoReceivesDeliveryErrors);
    }

    public function toString(): string
    {
        return $this->whoReceivesDeliveryErrors;
    }
}
