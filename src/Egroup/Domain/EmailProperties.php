<?php

namespace Glance\EgroupService\Egroup\Domain;

final class EmailProperties
{
    /** @var MailPostingRestrictions */
    private $mailPostingRestrictions;

    /** @var bool */
    private $senderAuthenticationEnabled;

    /** @var WhoReceivesDeliveryErrors */
    private $whoReceivesDeliveryErrors;

    /** @var MaxMailSize */
    private $maxMailSize;

    /** @var ArchiveProperties */
    private $archiveProperties;

    private function __construct(
        MailPostingRestrictions $mailPostingRestrictions,
        bool $senderAuthenticationEnabled,
        WhoReceivesDeliveryErrors $whoReceivesDeliveryErrors,
        MaxMailSize $maxMailSize,
        ArchiveProperties $archiveProperties
    ) {
        $this->mailPostingRestrictions = $mailPostingRestrictions;
        $this->senderAuthenticationEnabled = $senderAuthenticationEnabled;
        $this->whoReceivesDeliveryErrors = $whoReceivesDeliveryErrors;
        $this->maxMailSize = $maxMailSize;
        $this->archiveProperties = $archiveProperties;
    }

    public static function fromArray(array $input): self
    {
        return new self(
            MailPostingRestrictions::fromArray($input["MailPostingRestrictions"]),
            $input["SenderAuthenticationEnabled"],
            WhoReceivesDeliveryErrors::fromString($input["WhoReceivesDeliveryErrors"]),
            MaxMailSize::fromString($input["MaxMailSize"]),
            ArchiveProperties::fromString($input["ArchiveProperties"])
        );
    }

    public static function create(
        MailPostingRestrictions $mailPostingRestrictions,
        bool $senderAuthenticationEnabled,
        WhoReceivesDeliveryErrors $whoReceivesDeliveryErrors,
        MaxMailSize $maxMailSize,
        ArchiveProperties $archiveProperties
    ): self {
        return new self(
            $mailPostingRestrictions,
            $senderAuthenticationEnabled,
            $whoReceivesDeliveryErrors,
            $maxMailSize,
            $archiveProperties
        );
    }

    public static function createWithDefaultProperties(): self
    {
        return new self(
            MailPostingRestrictions::createWithDefaultProperties(),
            false,
            WhoReceivesDeliveryErrors::groupOwner(),
            MaxMailSize::tenMb(),
            ArchiveProperties::active()
        );
    }

    public function mailPostingRestrictions(): MailPostingRestrictions
    {
        return $this->mailPostingRestrictions;
    }

    public function updateMailPostingRestrictions(MailPostingRestrictions $mailPostingRestrictions): void
    {
        $this->mailPostingRestrictions = $mailPostingRestrictions;
    }

    public function senderAuthenticationEnabled(): bool
    {
        return $this->senderAuthenticationEnabled;
    }

    public function updateSenderAuthenticationEnabled(bool $senderAuthenticationEnabled): void
    {
        $this->senderAuthenticationEnabled = $senderAuthenticationEnabled;
    }

    public function whoReceivesDeliveryErrors(): WhoReceivesDeliveryErrors
    {
        return $this->whoReceivesDeliveryErrors;
    }

    public function updateWhoReceivesDeliveryErrors(WhoReceivesDeliveryErrors $whoReceivesDeliveryErrors): void
    {
        $this->whoReceivesDeliveryErrors = $whoReceivesDeliveryErrors;
    }

    public function maxMailSize(): MaxMailSize
    {
        return $this->maxMailSize;
    }

    public function updateMaxMailSize(MaxMailSize $maxMailSize): void
    {
        $this->maxMailSize = $maxMailSize;
    }

    public function archiveProperties(): ArchiveProperties
    {
        return $this->archiveProperties;
    }

    public function updateArchiveProperties(ArchiveProperties $archiveProperties): void
    {
        $this->archiveProperties = $archiveProperties;
    }

    public function toArray(): array
    {
        return [
            "MailPostingRestrictions" => $this->mailPostingRestrictions->toArray(),
            "SenderAuthenticationEnabled" => $this->senderAuthenticationEnabled,
            "WhoReceivesDeliveryErrors" => $this->whoReceivesDeliveryErrors->toString(),
            "MaxMailSize" => $this->maxMailSize->toString(),
            "ArchiveProperties" => $this->archiveProperties->toString(),
        ];
    }
}
