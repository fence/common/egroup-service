<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class Usage
{
    /** @var string */
    private $usage;

    /** @var string[] */
    public static $allowedPolicies = [
        "EgroupsOnly",
        "SecurityMailing",
    ];

    private function __construct(string $usage)
    {
        if (!in_array($usage, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Usage should be one of those values: {$allowed}"
            );
        }

        $this->usage = $usage;
    }

    public static function egroupsOnly(): self
    {
        return new self("EgroupsOnly");
    }

    public static function securityMailing(): self
    {
        return new self("SecurityMailing");
    }

    public static function fromString(string $usage): self
    {
        return new self($usage);
    }

    public function toString(): string
    {
        return $this->usage;
    }
}
