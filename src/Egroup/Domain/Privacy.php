<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class Privacy
{
    /** @var string */
    private $privacy;

    /** @var string[] */
    public static $allowedPolicies = [
        "Open",
        "Members",
        "Administrators",
        "Users"
    ];

    private function __construct(string $privacy)
    {
        if (!in_array($privacy, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Privacy should be one of those values: {$allowed}"
            );
        }

        $this->privacy = $privacy;
    }

    public static function open(): self
    {
        return new self("Open");
    }

    public static function members(): self
    {
        return new self("Members");
    }

    public static function administrators(): self
    {
        return new self("Administrators");
    }

    public static function users(): self
    {
        return new self("Users");
    }

    public static function fromString(string $privacy): self
    {
        return new self($privacy);
    }

    public function toString(): string
    {
        return $this->privacy;
    }
}
