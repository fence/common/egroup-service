<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class SelfSubscription
{
    /** @var string */
    private $selfSubscription;

    /** @var string[] */
    public static $allowedPolicies = [
        "Open",
        "Members",
        "Closed",
        "Users",
        "OpenWithAdminsAproval",
        "UsersWithAdminsAproval"
    ];

    private function __construct(string $selfSubscription)
    {
        if (!in_array($selfSubscription, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Self subscription should be one of those values: {$allowed}"
            );
        }

        $this->selfSubscription = $selfSubscription;
    }

    public static function open(): self
    {
        return new self("Open");
    }

    public static function members(): self
    {
        return new self("Members");
    }

    public static function closed(): self
    {
        return new self("Closed");
    }

    public static function users(): self
    {
        return new self("Users");
    }

    public static function openWithAdminsApproval(): self
    {
        return new self("OpenWithAdminsAproval");
    }

    public static function usersWithAdminsApproval(): self
    {
        return new self("UsersWithAdminsAproval");
    }

    public static function fromString(string $selfSubscription): self
    {
        return new self($selfSubscription);
    }

    public function toString(): string
    {
        return $this->selfSubscription;
    }
}
