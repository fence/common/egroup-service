<?php

namespace Glance\EgroupService\Egroup\Domain;

final class Owner
{
    private $personId;

    private function __construct(int $personId)
    {
        $this->personId = $personId;
    }

    public function personId(): string
    {
        return $this->personId;
    }

    public static function create(int $personId): self
    {
        return new self($personId);
    }

    public function toArray(): array
    {
        return [
            "PersonId" => $this->personId,
        ];
    }
}
