<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class PostingRestrictions
{
    /** @var string */
    private $postingRestrictions;

    /** @var string[] */
    public static $allowedPolicies = [
        "Everyone",
        "OwnerAdminsAndOthers"
    ];

    private function __construct(string $postingRestrictions)
    {
        if (!in_array($postingRestrictions, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Posting restrictions should be one of those values: {$allowed}"
            );
        }

        $this->postingRestrictions = $postingRestrictions;
    }

    public static function everyone(): self
    {
        return new self("Everyone");
    }

    public static function ownerAdminsAndOthers(): self
    {
        return new self("OwnerAdminsAndOthers");
    }

    public static function fromString(string $postingRestrictions): self
    {
        return new self($postingRestrictions);
    }

    public function toString(): string
    {
        return $this->postingRestrictions;
    }
}
