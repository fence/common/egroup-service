<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class MaxMailSize
{
    /** @var int */
    private $maxMailSize;

    /** @var string[] */
    public static $allowedPolicies = [
        1,
        2,
        5,
        10
    ];

    private function __construct(int $maxMailSize)
    {
        if (!in_array($maxMailSize, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Max mail size should be one of those values: {$allowed}"
            );
        }

        $this->maxMailSize = $maxMailSize;
    }

    public static function oneMb(): self
    {
        return new self(1);
    }

    public static function twoMb(): self
    {
        return new self(2);
    }

    public static function fiveMb(): self
    {
        return new self(5);
    }

    public static function tenMb(): self
    {
        return new self(10);
    }

    public static function fromString(string $maxMailSize): self
    {
        return new self($maxMailSize);
    }

    public function toString(): string
    {
        return $this->maxMailSize;
    }
}
