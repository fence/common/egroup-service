<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class Type
{
    /** @var string */
    private $type;

    /** @var string[] */
    public static $allowedPolicies = [
        "StaticEgroup",
        "DynamicEgroup",
    ];

    private function __construct(string $type)
    {
        if (!in_array($type, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Sync type should be one of those values: {$allowed}"
            );
        }

        $this->type = $type;
    }

    public static function staticEgroup(): self
    {
        return new self("StaticEgroup");
    }

    public static function dynamicEgroup(): self
    {
        return new self("DynamicEgroup");
    }

    public static function fromString(string $type): self
    {
        return new self($type);
    }

    public function toString(): string
    {
        return $this->type;
    }
}
