<?php

namespace Glance\EgroupService\Egroup\Domain;

use Glance\EgroupService\Shared\Domain\MapProperties;

final class MailPostingRestrictions
{
    /** @var PostingRestrictions */
    private $postingRestrictions;

    /** @var Person|StaticEgroup|DynamicEgroup[] */
    private $otherRecipientsAllowedToPost;

    private function __construct(
        PostingRestrictions $postingRestrictions,
        array $otherRecipientsAllowedToPost
    ) {
        $this->postingRestrictions = $postingRestrictions;
        $this->otherRecipientsAllowedToPost = $otherRecipientsAllowedToPost;
    }

    public static function fromArray(array $input): self
    {
        $otherRecipientAllowedToPost = [];
        if (isset($input["OtherRecipientsAllowedToPost"])) {
            $otherRecipientAllowedToPost = MapProperties::mapMembersArray($input["OtherRecipientsAllowedToPost"]);
        }
        return new self(
            PostingRestrictions::fromString($input["PostingRestrictions"]),
            $otherRecipientAllowedToPost
        );
    }

    public static function create(
        PostingRestrictions $postingRestrictions,
        array $otherRecipientsAllowedToPost
    ): self {
        return new self(
            $postingRestrictions,
            $otherRecipientsAllowedToPost
        );
    }

    public static function createWithDefaultProperties(): self
    {
        return new self(
            PostingRestrictions::everyone(),
            []
        );
    }

    public function postingRestrictions(): PostingRestrictions
    {
        return $this->postingRestrictions;
    }

    public function otherRecipientsAllowedToPost(): array
    {
        return $this->otherRecipientsAllowedToPost;
    }

    public function toArray(): array
    {
        return [
            "PostingRestrictions" => $this->postingRestrictions->toString(),
            "OtherRecipientsAllowedToPost" => array_map(function ($otherRecipientAllowedToPost) {
                return $otherRecipientAllowedToPost->toArray();
            }, $this->otherRecipientsAllowedToPost)
        ];
    }
}
