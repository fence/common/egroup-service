<?php

namespace Glance\EgroupService\Egroup\Domain;

final class Account
{
    private $type;
    private $name;

    public const TYPE_NAME = "Account";

    private function __construct(
        string $type,
        string $name
    ) {
        $this->type = $type;
        $this->name = $name;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function name(): string
    {
        return $this->name;
    }

    public static function create(string $name): self
    {
        return new self(self::TYPE_NAME, $name);
    }

    public function toArray(): array
    {
        return [
            "Name" => $this->name,
            "Type" => $this->type,
        ];
    }
}
