<?php

namespace Glance\EgroupService\Egroup\Domain;

final class SelfSubscriptionEgroup
{
    /**
     * @var int
     * The name of the e-group is not the ID of the e-group in this case.
     * The ID is the internal id of the e-group in the e-group service.
     */
    private $id;

    /**
     * @var bool
     */
    private $approvalNeeded;

    private function __construct(
        int $id,
        bool $approvalNeeded
    ) {
        $this->id = $id;
        $this->approvalNeeded = $approvalNeeded;
    }

    public static function fromArray(array $input): self
    {
        return new self(
            $input["ID"],
            $input["ApprovalNeeded"]
        );
    }

    public static function createWithApprovalNeededFromId(int $egroupId): self
    {
        return new self(
            $egroupId,
            true
        );
    }

    public static function createWithApprovalNotNeededFromId(int $egroupId): self
    {
        return new self(
            $egroupId,
            false
        );
    }

    public function id(): int
    {
        return $this->id;
    }

    public function approvalNeeded(): bool
    {
        return $this->approvalNeeded;
    }

    public function toArray(): array
    {
        return [
            "ID" => $this->id,
            "ApprovalNeeded" => $this->approvalNeeded,
        ];
    }
}
