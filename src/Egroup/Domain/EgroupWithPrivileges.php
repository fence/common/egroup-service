<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class EgroupWithPrivileges
{
    /** @var string */
    private $name;

    /** @var Privilege */
    private $privilege;

    private function __construct(
        string $name,
        Privilege $privilege
    ) {
        $this->name = $name;
        $this->privilege = $privilege;
    }

    public static function fromArray(array $input): self
    {
        return new self(
            $input["Name"],
            Privilege::fromString($input["Privilege"])
        );
    }

    public static function createWithAdminPrivilegeFromString(string $groupName): self
    {
        return new self(
            $groupName,
            Privilege::admin()
        );
    }

    public static function createWithSeeMembersPrivilegeFromString(string $groupName): self
    {
        return new self(
            $groupName,
            Privilege::seeMembers()
        );
    }

    public function name(): string
    {
        return $this->name;
    }

    public function privilege(): Privilege
    {
        return $this->privilege;
    }

    public function toArray(): array
    {
        return [
            "Name" => $this->name,
            "Privilege" => $this->privilege->toString(),
        ];
    }
}
