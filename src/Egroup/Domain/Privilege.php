<?php

namespace Glance\EgroupService\Egroup\Domain;

use InvalidArgumentException;

final class Privilege
{
    /** @var string */
    private $privilege;

    /** @var string[] */
    public static $allowedPolicies = [
        "SeeMembers",
        "Admin",
    ];

    private function __construct(string $privilege)
    {
        if (!in_array($privilege, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Privilege should be one of those values: {$allowed}"
            );
        }

        $this->privilege = $privilege;
    }

    public static function seeMembers(): self
    {
        return new self("SeeMembers");
    }

    public static function admin(): self
    {
        return new self("Admin");
    }

    public static function fromString(string $privilege): self
    {
        return new self($privilege);
    }

    public function toString(): string
    {
        return $this->privilege;
    }
}
