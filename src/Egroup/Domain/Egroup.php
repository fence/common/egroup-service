<?php

namespace Glance\EgroupService\Egroup\Domain;

use Glance\EgroupService\Shared\Domain\MapProperties;

final class Egroup
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var string
     */
    private $status;

    /**
     * @var Usage
     */
    private $usage;

    /**
     * @var string
     */
    private $topic;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Owner
     */
    private $owner;

    /**
     * @var string
     */
    private $adminEgroup;

    /**
     * @var Privacy
     */
    private $privacy;

    /**
     * @var SelfSubscription
     */
    private $selfSubscription;

    /**
     * @var Person|StaticEgroup|DynamicEgroup|Account[]
     */
    private $members;

    /**
     * @var EmailProperties
     */
    private $emailProperties;

    /**
     * @var EgroupWithPrivileges[]
     */
    private $egroupsWithPrivileges;

    /**
     * @var SelfSubscriptionEgroup[]
     * Can only be used if selfSubscription is set to "Members".
     * The name of the e-group is not the ID of the e-group in this case.
     * The ID is the internal id of the e-group in the e-group service.
     */
    private $selfSubscriptionEgroups;

    private function __construct(
        ?int $id,
        string $name,
        ?Type $type,
        ?string $status,
        ?Usage $usage,
        ?string $topic,
        ?string $description,
        Owner $owner,
        ?string $adminEgroup,
        ?Privacy $privacy,
        ?SelfSubscription $selfSubscription,
        array $members,
        ?EmailProperties $emailProperties,
        array $egroupsWithPrivileges,
        array $selfSubscriptionEgroups
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->status = $status;
        $this->usage = $usage;
        $this->topic = $topic;
        $this->description = $description;
        $this->owner = $owner;
        $this->adminEgroup = $adminEgroup;
        $this->privacy = $privacy;
        $this->selfSubscription = $selfSubscription;
        $this->members = $members;
        $this->emailProperties = $emailProperties;
        $this->egroupsWithPrivileges = $egroupsWithPrivileges;
        $this->selfSubscriptionEgroups = $selfSubscriptionEgroups;
    }

    public static function fromArray(array $array): self
    {
        $members = [];
        if (isset($array["Members"])) {
            $members = MapProperties::mapMembersArray($array["Members"]);
        }

        $egroupsWithPrivileges = [];
        if (isset($array["EgroupsWithPrivileges"])) {
            $egroupsWithPrivileges = MapProperties::mapEgroupsWithPrivilegesArray($array["EgroupsWithPrivileges"]);
        }

        $selfSubscriptionEgroups = [];
        if (isset($array["SelfsubscriptionEgroups"])) {
            $selfSubscriptionEgroups = MapProperties::mapSelfSubscriptionEgroupsArray($array["SelfsubscriptionEgroups"]);
        }

        return new self(
            isset($array["ID"]) ? (int) $array["ID"] : null,
            $array["Name"],
            isset($array["Type"]) ? Type::fromString($array["Type"]) : null,
            isset($array["Status"]) ? $array["Status"] : null,
            isset($array["Usage"]) ? Usage::fromString($array["Usage"]) : null,
            isset($array["Topic"]) ? $array["Topic"] : null,
            isset($array["Description"]) ? $array["Description"] : null,
            Owner::create($array["Owner"]["PersonId"]),
            isset($array["AdministratorEgroup"]) ? $array["AdministratorEgroup"] : null,
            isset($array["Privacy"]) ? Privacy::fromString($array["Privacy"]) : null,
            isset($array["Selfsubscription"]) ? SelfSubscription::fromString($array["Selfsubscription"]) : null,
            $members,
            isset($array["EmailProperties"]) ? EmailProperties::fromArray($array["EmailProperties"]) : [],
            $egroupsWithPrivileges,
            $selfSubscriptionEgroups
        );
    }

    public static function create(
        string $name,
        string $topic,
        Owner $owner,
        array $members,
        ?string $adminEgroup = null,
        ?string $description = null,
        ?Type $type = null,
        ?Usage $usage = null,
        ?Privacy $privacy = null,
        ?SelfSubscription $selfSubscription = null,
        ?EmailProperties $emailProperties = null,
        ?array $egroupsWithPrivileges = null,
        ?array $selfSubscriptionEgroups = null
    ): self {
        if ($type === null) {
            $type = Type::staticEgroup();
        }
        if ($usage === null) {
            $usage = Usage::securityMailing();
        }
        if ($privacy === null) {
            $privacy = Privacy::members();
        }
        if ($selfSubscription === null) {
            $selfSubscription = SelfSubscription::closed();
        }
        if ($emailProperties === null) {
            $emailProperties = EmailProperties::createWithDefaultProperties();
        }
        if ($egroupsWithPrivileges === null) {
            $egroupsWithPrivileges = [];
        }
        if ($selfSubscriptionEgroups === null) {
            $selfSubscriptionEgroups = [];
        }

        return new self(
            null,
            $name,
            $type,
            null,
            $usage,
            $topic,
            $description,
            $owner,
            $adminEgroup,
            $privacy,
            $selfSubscription,
            $members,
            $emailProperties,
            $egroupsWithPrivileges,
            $selfSubscriptionEgroups
        );
    }

    public function toArray(): array
    {
        return [
            "Id" => $this->id,
            "Name" => $this->name,
            "Type" => $this->type->toString(),
            "Status" => $this->status,
            "Usage" => $this->usage->toString(),
            "Topic" => $this->topic,
            "Description" => $this->description,
            "Owner" => $this->owner->toArray(),
            "AdministratorEgroup" => $this->adminEgroup,
            "Privacy" => $this->privacy->toString(),
            "Selfsubscription" => $this->selfSubscription->toString(),
            "Members" => array_map(function ($member) {
                return $member->toArray();
            }, $this->members),
            "EmailProperties" => $this->emailProperties->toArray(),
            "EgroupsWithPrivileges" => array_map(function ($egroupWithPrivileges) {
                return $egroupWithPrivileges->toArray();
            }, $this->egroupsWithPrivileges),
            "SelfsubscriptionEgroups" => array_map(function ($selfSubscriptionEgroup) {
                return $selfSubscriptionEgroup->toArray();
            }, $this->selfSubscriptionEgroups)
        ];
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function type(): ?Type
    {
        return $this->type;
    }

    public function status(): ?string
    {
        return $this->status;
    }

    public function usage(): ?Usage
    {
        return $this->usage;
    }

    public function topic(): ?string
    {
        return $this->topic;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function owner(): Owner
    {
        return $this->owner;
    }

    public function adminEgroup(): ?string
    {
        return $this->adminEgroup;
    }

    public function privacy(): ?Privacy
    {
        return $this->privacy;
    }

    public function selfSubscription(): ?SelfSubscription
    {
        return $this->selfSubscription;
    }

    public function members(): array
    {
        return $this->members;
    }

    /**
     * @var Person|StaticEgroup|DynamicEgroup[]
     */
    public function updateMembers(array $members): void
    {
        $this->members = $members;
    }

    public function emailProperties(): EmailProperties
    {
        return $this->emailProperties;
    }

    public function updateEmailProperties(EmailProperties $emailProperties): void
    {
        $this->emailProperties = $emailProperties;
    }

    public function egroupsWithPrivileges(): array
    {
        return $this->egroupsWithPrivileges;
    }

    public function updateEgroupsWithPrivileges(array $egroupsWithPrivileges): void
    {
        $this->egroupsWithPrivileges = $egroupsWithPrivileges;
    }

    public function selfSubscriptionEgroups(): array
    {
        return $this->selfSubscriptionEgroups;
    }

    public function updateSelfSubscriptionEgroups(array $selfSubscriptionEgroups): void
    {
        $this->selfSubscriptionEgroups = $selfSubscriptionEgroups;
    }
}
