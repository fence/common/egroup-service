<?php

namespace Glance\EgroupService\Egroup\Domain;

final class Person
{
    private $type;
    private $personId;

    public const TYPE_NAME = "Person";

    private function __construct(
        string $type,
        int $personId
    ) {
        $this->type = $type;
        $this->personId = $personId;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function personId(): string
    {
        return $this->personId;
    }

    public static function create(int $personId): self
    {
        return new self(self::TYPE_NAME, $personId);
    }

    public function toArray(): array
    {
        return [
            "ID" => $this->personId,
            "Type" => $this->type,
        ];
    }
}
