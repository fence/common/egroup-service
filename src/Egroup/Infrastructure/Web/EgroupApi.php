<?php

declare(strict_types=1);

namespace Glance\EgroupService\Egroup\Infrastructure\Web;

use DomainException;
use SoapClient;

/**
 * The WDSLcan be found on
 * https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl
 */
final class EgroupApi
{
    private const WDSL_URL = "https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl";

    private $client;

    private function __construct(SoapClient $client)
    {
        $this->client = $client;
    }

    public static function createWithUserCredentials(
        string $userLogin,
        string $userPassword
    ): self {
        return new self(
            new SoapClient(
                self::WDSL_URL,
                [
                    "login" => $userLogin,
                    "password" => $userPassword
                ]
            )
        );
    }

    public function findEgroupByName(string $name): array
    {
        $options = [
            "name" => $name
        ];

        try {
            $response = $this->client->FindEgroupByName(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }

    public function synchronizeEgroup(array $egroup): array
    {
        $options = [
            "egroup" => $egroup
        ];

        try {
            $response = $this->client->SynchronizeEgroup(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }

    public function updateEmailProperties(string $egroupName, array $emailProperties): array
    {
        $options = [
            "egroupName" => $egroupName,
            "emailProperties" => $emailProperties
        ];

        try {
            $response = $this->client->UpdateEmailProperties(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }

    public function removeEgroupMembers(string $egroupName, array $members): array
    {
        $options = [
            "egroupName" => $egroupName,
            "members" => $members,
        ];

        try {
            $response = $this->client->RemoveEgroupMembers(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }

    public function addEgroupMembers(
        string $egroupName,
        array $members,
        bool $overwriteMembers
    ): array {
        $options = [
            "egroupName" => $egroupName,
            "members" => $members,
            "overwriteMembers" => $overwriteMembers
        ];

        try {
            $response = $this->client->AddEgroupMembers(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }

    public function deleteEgroup(string $egroupName): array
    {
        $options = [
            "egroupName" => $egroupName,
        ];

        try {
            $response = $this->client->deleteEgroup(
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }

        return json_decode(json_encode($response), true);
    }
}
