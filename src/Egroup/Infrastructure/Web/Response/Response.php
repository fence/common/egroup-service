<?php

declare(strict_types=1);

namespace Glance\EgroupService\Egroup\Infrastructure\Web\Response;

use Glance\EgroupService\Egroup\Domain\Egroup;
use Glance\EgroupService\Egroup\Infrastructure\Web\Exception\EgroupException;

final class Response
{
    private $transactionId;
    private $warnings;
    private $egroup;

    private function __construct(
        string $transactionId,
        string $warnings = null,
        ?Egroup $egroup = null
    ) {
        $this->transactionId = $transactionId;
        $this->egroup = $egroup;
        $this->warnings = $warnings;
    }

    public static function create(array $response): self
    {
        if (isset($response["error"])) {
            throw self::toException($response["error"]);
        }

        $warnings = null;
        if (isset($response["warnings"])) {
            $warnings = self::handleWarnings($response["warnings"]);
        }

        $egroup = null;
        if (isset($response["result"])) {
            $egroup = Egroup::fromArray($response["result"]);
        }

        return new self(
            $response["transactionId"],
            $warnings,
            $egroup
        );
    }

    public function toArray(): array
    {
        return [
            "transactionId" => $this->transactionId,
            "warnings" => $this->warnings,
            "egroup" => $this->egroup
        ];
    }

    public function transactionId(): string
    {
        return $this->transactionId;
    }

    public function warnings(): ?string
    {
        return $this->warnings;
    }

    public function egroup(): ?Egroup
    {
        return $this->egroup;
    }

    private function toException(array $error): EgroupException
    {
        throw EgroupException::withDefaultMessage($error);
    }

    private function handleWarnings(array $warnings): string
    {
        if (isset($warnings["Message"])) {
            return $warnings["Message"];
        }

        $message = "";
        foreach ($warnings as $warning) {
            $message .= $warning["Message"] . "\n";
        }
        return $message;
    }
}
