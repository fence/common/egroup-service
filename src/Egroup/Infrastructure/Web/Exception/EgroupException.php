<?php

declare(strict_types=1);

namespace Glance\EgroupService\Egroup\Infrastructure\Web\Exception;

use Exception;

/**
 * Schema can be found on
 * https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsServicesSchema.xsd
 *
 * "INSUFFICIENT_PRIVILEGES"
 * "NOT_VALID_USER"
 * "NOT_FOUND"
 * "EGROUP_NAME_BAD_FORMATED"
 * "PERSON_ID_NOT_FOUND"
 * "EXPIRE_DATE_BAD_FORMATTED"
 * "NAME_AND_ID_NOT_CORRESPONDS"
 * "EGROUP_RECENTLY_DELETED"
 * "EMAIL_PROP_ARCHIVE_NOT_VALID"
 * "EMAIL_PROP_MAIL_SIZE_NOT_VALID"
 * "EXPIRATION_DATE_MANDATORY"
 * "EXPIRATION_DATE_NOT_VALID"
 * "INTERNAL_DB_ERROR"
 * "DATABASE_CONFIGURATION_NOT_FOUND"
 * "EGROUP_MEMBER_OF_ANOTHER"
 * "NOT_MAILING_SEGURITY_USAGE"
 * "IS_ALREADY_MEMBER"
 * "TOPIC_ALREADY_EXISTS"
 * "ALIAS_MUST_HAVE_HYPHEN"
 * "ALIAS_ALREADY_EXISTS"
 * "NAME_ALREADY_RESERVED"
 * "NAME_TOO_LONG"
 * "EGROUP_ALREADY_EXISTS"
 * "MEMBER_NOT_FOUND"
 * "ALIAS_NOT_FOUND"
 * "SELF_EGROUP_ALREADY_EXISTS"
 * "ALREADY_ACTIVE"
 * "IS_BLOCKED"
 * "MUST_BE_MODERATOR"
 * "STATUS_CHANGE_NOT_ALLOWED"
 * "EXPIRATION_DATE_CANT_BE_PROLONGUED"
 * "BLOCKING_REASON_UNDEFINED"
 * "USAGE_TYPE_NOT_VALID"
 * "NOT_LOGGED"
 * "ALREADY_DELETED"
 * "NAME_MUST_HAVE_HYPHEN"
 * "IS_ALREADY_ALLOWED_TO_POST"
 * "HAS_ALREADY_PRIVILEGE"
 * "OWNER_ID_NOT_FOUND"
 * "WARNING"
 * "UNEXPECTED_ERROR"
 */

final class EgroupException extends Exception
{
    public static function withDefaultMessage(array $error): self
    {
        return new self(
            self::getErrorMessage($error)
        );
    }

    public static function getErrorMessage(array $error): string
    {
        $code = isset($error["Code"]) ? $error["Code"] : "";
        $message = isset($error["Message"]) ? $error["Message"] : "";
        $field = isset($error["Field"]) ? $error["Field"] : "";

        return $field ? $code . ": " . $message . ". Field: " . $field : $code . ": " . $message;
    }
}
