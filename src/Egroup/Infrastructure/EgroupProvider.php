<?php

declare(strict_types=1);

namespace Glance\EgroupService\Egroup\Infrastructure;

use Glance\EgroupService\Egroup\Infrastructure\Web\EgroupApi;
use Glance\EgroupService\Egroup\Domain\Egroup;
use Glance\EgroupService\Egroup\Infrastructure\Web\Response\Response;

final class EgroupProvider
{
    /** @var EgroupApi */
    private $groupApi;

    final private function __construct(
        string $userLogin,
        string $userPassword
    ) {
        $options = [
            "groupApi" => EgroupApi::createWithUserCredentials(
                $userLogin,
                $userPassword
            ),
        ];

        $this->groupApi = $options["groupApi"];
    }

    public static function createWithUserCredentials(
        string $userLogin,
        string $userPassword
    ): self {
        return new self(
            $userLogin,
            $userPassword
        );
    }

    public function findEgroupByName(string $egroupName): Response
    {
        $response = $this->groupApi->findEgroupByName($egroupName);

        return Response::create($response);
    }

    public function synchronizeEgroup(Egroup $egroup): Response
    {
        $response = $this->groupApi->synchronizeEgroup($egroup->toArray());

        return Response::create($response);
    }

    public function updateEmailProperties(Egroup $egroup): Response
    {
        $response = $this->groupApi->updateEmailProperties(
            $egroup->name(),
            $egroup->emailProperties()->toArray()
        );

        return Response::create($response);
    }

    public function removeEgroupMembers(string $egroupName, array $members): Response
    {
        $members = array_map(function (object $member) {
            return $member->toArray();
        }, $members);

        $response = $this->groupApi->removeEgroupMembers(
            $egroupName,
            $members
        );

        return Response::create($response);
    }

    public function addEgroupMembers(
        string $egroupName,
        array $members,
        bool $overwriteMembers = false
    ): Response {
        $members = array_map(function (object $member) {
            return $member->toArray();
        }, $members);

        $response = $this->groupApi->addEgroupMembers(
            $egroupName,
            $members,
            $overwriteMembers
        );

        return Response::create($response);
    }

    public function delete(string $egroupName): Response
    {
        $response = $this->groupApi->deleteEgroup($egroupName);

        return Response::create($response);
    }
}
