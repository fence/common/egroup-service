<?php

namespace Glance\EgroupService\Shared\Domain;

use Glance\EgroupService\Egroup\Domain\Account;
use Glance\EgroupService\Egroup\Domain\DynamicEgroup;
use Glance\EgroupService\Egroup\Domain\EgroupWithPrivileges;
use Glance\EgroupService\Egroup\Domain\Person;
use Glance\EgroupService\Egroup\Domain\SelfSubscriptionEgroup;
use Glance\EgroupService\Egroup\Domain\StaticEgroup;

final class MapProperties
{
    public static function mapMembersArray(array $members): array
    {
        // Checks for the cases when there is only one member in the e-group.
        if (isset($members["Name"])) {
            return [self::mapMember($members)];
        }
        return array_map(function ($member) {
            return self::mapMember($member);
        }, $members);
    }

    public static function mapMember(array $member)
    {
        if ($member["Type"] === Person::TYPE_NAME) {
            return Person::create($member["ID"]);
        }
        if ($member["Type"] === StaticEgroup::TYPE_NAME) {
            return StaticEgroup::create($member["Name"]);
        }
        if ($member["Type"] === DynamicEgroup::TYPE_NAME) {
            return DynamicEgroup::create($member["Name"]);
        }
        if ($member["Type"] === Account::TYPE_NAME) {
            return Account::create($member["Name"]);
        }
    }

    public static function mapEgroupsWithPrivilegesArray(array $egroupsWithPrivileges): array
    {
        // Checks for the cases when there is only one e-group.
        if (isset($egroupsWithPrivileges["Name"])) {
            return [EgroupWithPrivileges::fromArray($egroupsWithPrivileges)];
        }
        return array_map(function ($egroupWithPrivileges) {
            return EgroupWithPrivileges::fromArray($egroupWithPrivileges);
        }, $egroupsWithPrivileges);
    }

    public static function mapSelfSubscriptionEgroupsArray(array $selfSubscriptionEgroups): array
    {
        // Checks for the cases when there is only one e-group.
        if (isset($selfSubscriptionEgroups["ID"])) {
            return [SelfSubscriptionEgroup::fromArray($selfSubscriptionEgroups)];
        }
        return array_map(function ($selfSubscriptionEgroup) {
            return SelfSubscriptionEgroup::fromArray($selfSubscriptionEgroup);
        }, $selfSubscriptionEgroups);
    }
}
